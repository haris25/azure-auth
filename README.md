# README

To run the project:

1. Get Azure Service Principal credentials (see https://docs.microsoft.com/en-us/cli/azure/create-an-azure-service-principal-azure-cli?view=azure-cli-latest)

2. Set the following environment variables:
   AZURE_CLIENT_ID: **appId** from Service Principal creation output.

   AZURE_SECRET: **password** from Service Principal creation output.

   AZURE_SUBSCRIPTION_ID: the subscription Id for the project.

   AZURE_TENANT: the tenant id for the project.

   AZURE_DEFAULT_LOCATION: one valid Azure location, e.g. australiaeast

   ```
   AZURE_CLIENT_ID=12345678-abcd-9012-efgh-345678901234
   AZURE_SECRET=super_secret_password
   AZURE_SUBSCRIPTION_ID=abcdefgh-0123-ijkl-4567-mnopqrstuvwx
   AZURE_TENANT=0e5206d0-86d9-40bd-84fe-dddddddddddd
   AZURE_DEFAULT_LOCATION='australiaeast'
   export AZURE_CLIENT_ID AZURE_SECRET AZURE_SUBSCRIPTION_ID \
     AZURE_TENANT AZURE_DEFAULT_LOCATION
   ```

   

3. To setup ansible, run:

   ```
   virtualenv .venv -p /usr/bin/python3
   source .venv/bin/activate
   pip install -r requirements.txt
   ```

4. Execute ansible:

   ```
   ansible-playbook -i localhost sites.yml -v
   ```

   